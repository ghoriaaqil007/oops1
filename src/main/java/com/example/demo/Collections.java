package com.example.demo;

import java.util.*;

public class Collections {
    public static void main(String[] args) {
        // ArrayList
        List<String> arrayList = new ArrayList<>();
        arrayList.add("Apple");
        arrayList.add("Banana");
        arrayList.add("Cherry");
        System.out.println("ArrayList: " + arrayList);

        // LinkedList
        List<String> linkedList = new LinkedList<>();
        linkedList.add("Apple");
        linkedList.add("Banana");
        linkedList.add("Cherry");
        System.out.println("LinkedList: " + linkedList);

        // HashSet
        Set<String> hashSet = new HashSet<>();
        hashSet.add("Red");
        hashSet.add("Green");
        hashSet.add("Blue");
        hashSet.add("Red"); // This will not be added because sets do not allow duplicate elements
        System.out.println("HashSet: " + hashSet);

        // TreeSet
        Set<String> treeSet = new TreeSet<>();
        treeSet.add("London");
        treeSet.add("Paris");
        treeSet.add("New York");
        System.out.println("TreeSet: " + treeSet); // This will print the elements in sorted order

        // HashMap
        Map<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Alice", 25);
        hashMap.put("Bob", 30);
        hashMap.put("Charlie", 35);
        System.out.println("HashMap: " + hashMap);

        // LinkedHashMap
        Map<String, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("One", "Apple");
        linkedHashMap.put("Two", "Banana");
        linkedHashMap.put("Three", "Cherry");
        System.out.println("LinkedHashMap: " + linkedHashMap); // This will maintain the insertion order
    }
}

package com.example.demo;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

class Book {
    private String title;
    private String author;
    private int year;

    public Book(String title, String author, int year) {
        this.title = title;
        this.author = author;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", year=" + year +
                '}';
    }
}

public class Library {
    private List<Book> books;

    public Library(List<Book> books) {
        this.books = books;
    }

    public Optional<Book> findBook(Predicate<Book> searchCriteria) {
        return books.stream()
                .filter(searchCriteria)
                .findFirst();
    }

    public static void main(String[] args) {
        List<Book> books = Arrays.asList(
                new Book("The Lord of the Rings", "J.R.R. Tolkien", 1954),
                new Book("To Kill a Mockingbird", "Harper Lee", 1960),
                new Book("1984", "George Orwell", 1949),
                new Book("Pride and Prejudice", "Jane Austen", 1813)
        );

        Library library = new Library(books);

        Predicate<Book> isOldBook = book -> book.getYear() < 1950;
        System.out.println(isOldBook);
        Optional<Book> oldBook = library.findBook(isOldBook);

        oldBook.ifPresentOrElse(
                System.out::println,
                () -> System.out.println("No old book found.")
        );

        Predicate<Book> isTolkienBook = book -> book.getAuthor().equals("J.R.R. Tolkien");
        Optional<Book> tolkienBook = library.findBook(isTolkienBook);

        tolkienBook.ifPresentOrElse(
                System.out::println,
                () -> System.out.println("No Tolkien book found.")
        );
    }
}

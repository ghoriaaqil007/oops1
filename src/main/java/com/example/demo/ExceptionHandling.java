package com.example.demo;

class CustomException extends Exception {
    CustomException(String s) {
        super(s);
    }
}

public class ExceptionHandling {
    static void checkAge(int age) throws CustomException {
        if(age < 18) {
            throw new CustomException("Age less than 18");
        } else {
            System.out.println("Access granted");
        }
    }

    public static void main(String[] args) {
        try {
            // This will throw an ArithmeticException
            int division = 10 / 0;
        } catch (ArithmeticException e) {
            System.out.println("Caught ArithmeticException: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Caught Exception: " + e.getMessage());
        } finally {
            System.out.println("This will always run");
        }

        try {
            // This will throw a CustomException
            checkAge(15);
        } catch (CustomException e) {
            System.out.println("Caught CustomException: " + e.getMessage());
        }
    }
}

package com.example.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Person {
    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String toString() {
        return "Name: " + this.name + ", Age: " + this.age;
    }
}

class PersonComparator implements Comparator<Person> {
    public int compare(Person p1, Person p2) {
        int nameCompare = p1.name.compareTo(p2.name);
        int ageCompare = p2.age - p1.age; // for descending order

        if (nameCompare == 0) {
            return ((ageCompare == 0) ? nameCompare : ageCompare);
        } else {
            return nameCompare;
        }
    }
}

public class CustomComparator {
    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Alice", 25));
        people.add(new Person("Bob", 30));
        people.add(new Person("Charlie", 35));
        people.add(new Person("Alice", 30));

        Collections.sort(people, new PersonComparator());

        for (Person person : people) {
            System.out.println(person);
        }
    }
}

package com.example.demo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

@Component
public class BeanLifeCycle {

    private Connection connection;

    @PostConstruct
    public void initialize() {
        System.out.println("Initializing database connection");

        try {
            connection = DriverManager.getConnection("jdbc:myDriver:myDatabase");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void executeQuery(String query) {
        // Use the connection to execute the query
    }

    @PreDestroy
    public void close() {
        System.out.println("Closing database connection");

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
package com.example.demo;

import java.util.Optional;

public class Optionals {
    public static void main(String[] args) {
        // Optional.of()
        Optional<String> optional = Optional.of("Hello, world!");
        System.out.println(optional.get()); // "Hello, world!"

        // Optional.empty()
        Optional<String> emptyOptional = Optional.empty();
        System.out.println(emptyOptional.isPresent()); // false

        // Optional.ofNullable()
        Optional<String> nullableOptional = Optional.ofNullable(null);
        System.out.println(nullableOptional.isPresent()); // false

        // Optional.isPresent() and Optional.ifPresent()
        Optional<String> optional1 = Optional.of("Hello, world!");
        if (optional1.isPresent()) {
            System.out.println(optional1.get()); // "Hello, world!"
        }
        optional1.ifPresent(System.out::println); // "Hello, world!"

        // Optional.orElse()
        Optional<String> optional2 = Optional.ofNullable(null);
        System.out.println(optional2.orElse("Default value")); // "Default value"

        // Optional.orElseGet()
        Optional<String> optional3 = Optional.ofNullable(null);
        System.out.println(optional3.orElseGet(() -> "Generated default value")); // "Generated default value"

        // Optional.orElseThrow()
        Optional<String> optional4 = Optional.ofNullable(null);
        try {
            System.out.println(optional4.orElseThrow(() -> new Exception("Exception: Optional is empty")));
        } catch (Exception e) {
            System.out.println(e.getMessage()); // "Exception: Optional is empty"
        }
    }
}

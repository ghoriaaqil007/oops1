package com.example.demo;

import java.util.*;
import java.util.stream.*;

public class Java8Streams {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("John", "Jane", "Jeremy", "John", "Jamie");

        // Filter
        Stream<String> filteredNames = names.stream().filter(name -> name.startsWith("J"));
        filteredNames.forEach(System.out::print); // prints all names starting with "J"
System.out.println();
        // Filter Chaining
        long count = names.stream().filter(name -> name.startsWith("J")).filter(name -> name.length() == 4).count();
        System.out.println(count); // prints count of names starting with "J" and of length 4

        // Sorted
        List<String> sortedNames = names.stream().sorted().collect(Collectors.toList());
        System.out.println(sortedNames); // prints names sorted in natural order

        // GroupBy
        Map<Integer, List<String>> namesByLength = names.stream().collect(Collectors.groupingBy(String::length));
        System.out.println(namesByLength); // prints names grouped by their length

        // Map
        List<Integer> nameLengths = names.stream().map(String::length).collect(Collectors.toList());
        System.out.println(nameLengths); // prints lengths of all names

        // FlatMap
        List<String> distinctChars = names.stream().map(name -> name.split("")).flatMap(Arrays::stream).distinct().collect(Collectors.toList());
        System.out.println(distinctChars); // prints distinct characters used in all names

        // FindAny
        Optional<String> anyName = names.stream().filter(name -> name.startsWith("J")).findAny();
        anyName.ifPresent(System.out::println); // prints a name starting with "J", if any
    }
}

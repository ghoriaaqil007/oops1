package com.example.demo;

import java.util.function.Predicate;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.function.Function;

public class Functionalnterfaces {

    public static void main(String[] args) {
        // Predicate example
        Predicate<Integer> isEven = number -> number % 2 == 0;
        System.out.println("Predicate Result: " + isEven.test(10));  // Output: true

        // Consumer example
        Consumer<String> printUpperCase = str -> System.out.println("Consumer Result: " + str.toUpperCase());
        printUpperCase.accept("Hello, World!");  // Output: HELLO, WORLD!

        // Supplier example
        Supplier<Double> randomValue = () -> Math.random();
        System.out.println("Supplier Result: " + randomValue.get());  // Output: A random value between 0.0 (inclusive) and 1.0 (exclusive)

        // Function example
        Function<String, Integer> lengthFunction = str -> str.length();
        System.out.println("Function Result: " + lengthFunction.apply("Hello, World!"));  // Output: 13
    }
}

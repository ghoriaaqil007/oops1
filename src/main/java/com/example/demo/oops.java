package com.example.demo;

//Abstarct class
abstract class Animal {
    private String name;

    Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
 // Method overloading
    public void eat() {
        System.out.println(getName() + " is eating");
    }

    public void eat(String food) {
        System.out.println(getName() + " is eating " + food);
    }

    abstract void sound();
}

// Inheritance
class Dog extends Animal {
    Dog(String name) {
        super(name);
    }

    // Polymorphism (Method Overriding)
    @Override
    void sound() {
        System.out.println("Dog " + getName() + " says: Woof Woof!");
    }
}

class Cat extends Animal {
    Cat(String name) {
        super(name);
    }

    // Polymorphism (Method Overriding)
    @Override
    void sound() {
        System.out.println("Cat " + getName() + " says: Meow Meow!");
    }
}

public class oops {
    public static void main(String[] args) {
        // Creating Objects
        Dog myDog = new Dog("Buddy");
        Cat myCat = new Cat("Lucy");

        // Polymorphism (Method Overriding)
        makeSound(myDog);
        makeSound(myCat);
        
        // Polymorphism (Method OverLoading)
        myDog.sound();
        myDog.eat();
        myDog.eat("bone");

        myCat.sound();
        myCat.eat();
        myCat.eat("fish");
    }

    public static void makeSound(Animal animal) {
        animal.sound();
    }
}

/*We create an ExecutorService with a single thread. This is like having a single worker.

We create a Runnable task and give it to the worker. The task simply prints a message saying it was executed.

We create a Callable task and give it to the worker. This task waits for a second (to simulate doing some work) and then returns a message saying it was executed. We get a Future from the ExecutorService when we submit the task. We then print the result of the Future once it's ready.

We create a CompletableFuture task that also waits for a second and then prints a message. We wait for this task to complete before moving on.

We create a ConcurrentHashMap, put two entries in it, and print it.

We create a CopyOnWriteArrayList, add two elements to it, and print it.

We create a BlockingQueue with a capacity of 5, put an element in it, then take the element out and print it.

Finally, we shut down the ExecutorService after all tasks are done
*/
package com.example.demo;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MultiThreading {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        // ExecutorService with a single thread
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        // Runnable task
        Runnable runnableTask = () -> System.out.println("Runnable: Executed by " + Thread.currentThread().getName());
        executorService.execute(runnableTask);

        // Callable task with Future
        Callable<String> callableTask = () -> {
            Thread.sleep(1000); // Simulate some delay
            return "Callable: Executed by " + Thread.currentThread().getName();
        };
        Future<String> future = executorService.submit(callableTask);
        System.out.println(future.get());

        // CompletableFuture task
        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(() -> {
            try {
                Thread.sleep(1000); // Simulate some delay
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("CompletableFuture: Executed by " + Thread.currentThread().getName());
        }, executorService);
        completableFuture.get(); // Wait for the CompletableFuture to complete

        // ConcurrentHashMap
        ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap<>();
        concurrentHashMap.put("Key1", "Value1");
        concurrentHashMap.put("Key2", "Value2");
        System.out.println("ConcurrentHashMap: " + concurrentHashMap);

        // CopyOnWriteArrayList
        CopyOnWriteArrayList<String> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
        copyOnWriteArrayList.add("Element1");
        copyOnWriteArrayList.add("Element2");
        System.out.println("CopyOnWriteArrayList: " + copyOnWriteArrayList);

        // BlockingQueue
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(5);
        blockingQueue.put("Element3");
        System.out.println("BlockingQueue: " + blockingQueue.take());

        // Shutdown ExecutorService
        executorService.shutdown();
    }
}

package com.example.demo;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeExample {
    public static void main(String[] args) {
        LocalDate date = LocalDate.now();
        System.out.println("Current Date: " + date);

        LocalTime time = LocalTime.now();
        System.out.println("Current Time: " + time);

        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println("Current Date & Time: " + dateTime);

        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        System.out.println("Current Date, Time & Timezone: " + zonedDateTime);

        Instant timestamp = Instant.now();
        System.out.println("Current Timestamp: " + timestamp);

        Period period = Period.between(LocalDate.of(2020, 1, 1), LocalDate.of(2023, 1, 1));
        System.out.println("Period: " + period.getYears() + " Years");

        Duration duration = Duration.between(LocalTime.of(10, 20), LocalTime.of(16, 30));
        System.out.println("Duration: " + duration.toHours() + " Hours");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        System.out.println("Formatted DateTime: " + dateTime.format(formatter));
    }
}
